import App from './src/App.js'

window.onload = function () {
  ReactDOM.render(
    React.createElement(App),
    document.querySelector('#main')
  )
}
