// See https://unicode.org/Public/emoji/13.0/emoji-test.txt
const EMOJI = {
  'Smileys & Emotion': {
    '😀': 'grinning face',
    '😃': 'grinning face with big eyes',
    '😄': 'grinning face with smiling eyes',
    '😁': 'beaming face with smiling eyes',
    '😆': 'grinning squinting face',
    '😅': 'grinning face with sweat',
    '🤣': 'rolling on the floor laughing',
    '😂': 'face with tears of joy',
    '🙂': 'slightly smiling face',
    '🙃': 'upside-down face',
    '😉': 'winking face',
    '😊': 'smiling face with smiling eyes',
    '😇': 'smiling face with halo'
  },
  'People & Body': {
    '👋': 'waving hand',
    '👋🏻': 'waving hand: light skin tone',
    '👋🏼': 'waving hand: medium-light skin tone',
    '👋🏽': 'waving hand: medium skin tone',
    '👋🏾': 'waving hand: medium-dark skin tone',
    '👋🏿': 'waving hand: dark skin tone'
  }
}

export default EMOJI
