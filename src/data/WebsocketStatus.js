// See https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent
const WEBSOCKET_STATUS = {
  '1000': '', // Normal closure, so no error message.
  '1001': 'Websocket Error: Going Away',
  '1002': 'Websocket Error: Protocol Error',
  '1003': 'Websocket Error: Unsupported Data',
  '1004': 'Unknown Websocket Error', // Currently undefined; shouldn't happen.
  '1005': '', // "No Status Received" - The Ruby "em-websocket" gem does this.
  '1006': 'Could not connect to websocket server', // "Abnormal Closure"
  '1007': 'Websocket Error: Invalid frame payload data',
  '1008': 'Websocket Error: Policy Violation',
  '1009': 'Websocket Error: Message too big',
  '1010': 'Websocket Error: Missing Extension',
  '1011': 'Websocket Error: Internal Error',
  '1012': 'Websocket Error: Service Restart',
  '1013': 'Websocket Error: Try Again Later',
  '1014': 'Websocket Error: Bad Gateway',
  '1015': 'Websocket Error: TLS Handshake'
}

export default WEBSOCKET_STATUS
