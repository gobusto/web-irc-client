// See https://tools.ietf.org/html/rfc1459
const IRC_CODE = {
  'AWAY_AUTOREPLY': '301',
  'AWAY_OFF': '305',
  'AWAY_ON': '306',
  'WHOIS_USER': '311',
  'CHANNEL_LIST_ITEM': '322',
  'CHANNEL_LIST_END': '323',
  'CHANNEL_MODE': '324',
  'TOPIC_TEXT': '332',
  'NAME_LIST': '353',
  'BAN_LIST_ITEM': '367',
  'MOTD_LINE': '372',
  'MOTD_END': '376',
  'NO_MOTD': '422'
}

// This class handles the actual networking stuff for talking to an IRC server.
class IrcConnection {
  constructor (params) {
    const url = new URL(params.proxy)
    url.searchParams.append('host', params.host || '')
    url.searchParams.append('port', params.port || '')

    this.url = url.toString()
    this.nick = params.nick || ''
    this.name = params.name || 'Anonymous'

    // We need to store this for automatic reconnections:
    this.pass = params.pass || ''

    this._connect()
  }

  getChannels () {
    if (this.gettingChannelList) { return }
    this.gettingChannelList = true
    this.channelList = {}

    this._send('LIST')
  }

  getChannelFlags (channel) {
    this._send(`MODE ${channel}`)
  }

  getChannelBans (channel) {
    this._send(`MODE ${channel} +b`) // ircd-hybrid also accepts -b and b here.
  }

  setTopicProtected (channel, enabled) {
    this._send(`MODE ${channel} ${enabled ? '+t' : '-t'}`)
  }

  setVoicedOnly (channel, enabled) {
    this._send(`MODE ${channel} ${enabled ? '+m' : '-m'}`)
  }

  setInviteOnly (channel, enabled) {
    this._send(`MODE ${channel} ${enabled ? '+i' : '-i'}`)
  }

  setMembersOnly (channel, enabled) {
    this._send(`MODE ${channel} ${enabled ? '+n' : '-n'}`)
  }

  // NOTE: "private" is considered obsolete - "secret" is the preferred option.
  setHideChannel (channel, enabled) {
    this._send(`MODE ${channel} ${enabled ? '+s' : '-sp'}`)
  }

  whoIs (nick) {
    this._send(`WHOIS ${nick}`)
  }

  join (channel) {
    this._send(`JOIN ${channel}`)
  }

  leave (channel) {
    this._send(`PART ${channel}`)
  }

  setTopic (channel, topic) {
    this._send(`TOPIC ${channel} :${topic || ''}`)
  }

  send (channel, text) {
    this._send(`PRIVMSG ${channel} :${text}`)

    if (!this.onMessage) {
      return
    } else if (this._validChannelName(channel)) {
      return this.onMessage('public', 'normal', this.nick, text, channel)
    }

    this.onMessage('private', 'normal', this.nick, text, channel)
  }

  away (message) {
    this._send(`AWAY ${message || 'Away'}`)
  }

  back () {
    this._send('AWAY')
  }

  disconnect () {
    this._send('QUIT')
  }

  _connect () {
    this.ready = false // Sending things before a server is ready may not work.
    this.buffer = '' // We might not always get a complete line in a single go.

    this.socket = new WebSocket(this.url)
    this.socket.onopen = this._onOpen.bind(this)
    this.socket.onmessage = this._onReceiveData.bind(this)
    this.socket.onclose = this._onClose.bind(this)
  }

  _onOpen (event) {
    console.debug('Open')

    if (this.pass) { this._send(`PASS ${this.pass}`) }
    this._send(`NICK ${this.nick}`)
    this._send(`USER ${this.nick} * * :${this.name}`)

    if (this.onConnect) { this.onConnect() }
  }

  _onReceiveData (event) {
    if (typeof(event.data) === 'string') { return this._readText(event.data) }

    // Websockify sends us a binary blob, rather than a plain text string:
    event.data.text().then(text => this._readText(text))
  }

  _readText (text) {
    console.debug(text)
    this.buffer += text                   // "FOO\r" --> "FOO\r\nBAR\r\n"
    const lines = this.buffer.split('\n') // ["FOO\r", "BAR\r", ""]
    this.buffer = lines.pop()             // ["FOO\r", "BAR\r"] (buffer is "")

    lines.forEach(line => this._processLine(line))
  }

  _processLine (line) {
    if (line.startsWith('PING ')) {
      return this._send(`PONG ${location.hostname}`)
    } else if (line.startsWith('ERROR ')) {
      return this._onError(line)
    } else if (!line.startsWith(':')) {
      return // Unrecognised.
    }

    // :gobusto!~gobusto@95.150.81.37 JOIN #general
    const mask = line.split(' ')[0]
    const code = line.split(' ')[1]
    const text = line.slice(`${mask} ${code} `.length).replace('\r', '')
    const nick = mask.slice(1).split('!')[0] // Remove : prefix + any ! suffix.

    if (!this.ready && [IRC_CODE.MOTD_END, IRC_CODE.NO_MOTD].includes(code)) {
      this.ready = true
      if (this.onReady) { this.onReady() }
    } else if (code === IRC_CODE.AWAY_ON) {
      if (this.onUserFlagChange) { this.onUserFlagChange(this.nick, 'away', true) }
    } else if (code === IRC_CODE.AWAY_OFF) {
      if (this.onUserFlagChange) { this.onUserFlagChange(this.nick, 'away', false) }
    } else if (code === IRC_CODE.TOPIC_TEXT) {
      this._onTopic(text)
    } else if (code === IRC_CODE.BAN_LIST_ITEM) {
      this._onBanListItem(text)
    } else if (code === IRC_CODE.CHANNEL_MODE) {
      this._onChannelMode(text)
    } else if (code === 'MODE') {
      this._onModeChange(text)
    } else if (code === IRC_CODE.WHOIS_USER) {
      this._onWhoIs(text)
    } else if (code === IRC_CODE.CHANNEL_LIST_ITEM) {
      this._onChannelListItem(text)
    } else if (code === IRC_CODE.CHANNEL_LIST_END) {
      this._onChannelListEnd()
    } else if (code === IRC_CODE.NAME_LIST) {
      this._onNameList(text)
    } else if (code === IRC_CODE.AWAY_AUTOREPLY) {
      this._onAutoReply(text)
    } else if (code === 'NICK') {
      this._onNick(nick, text)
    } else if (code === 'JOIN') {
      this._onJoin(nick, text)
    } else if (code === 'PART') {
      this._onLeave(nick, text)
    } else if (code) {
      this._onMessage(mask.includes('!') ? nick : null, code, text)
    }
  }

  // ERROR :Closing Link: localhost (You are not authorized to use this server)
  // ERROR :Closing Link: bonbori[~a@b.c] gobusto (Local Kill by gobusto (abc))
  _onError (line) {
    if (!this.onError) { return }

    let text = line.slice(6)
    if (text.startsWith(':')) { text = text.slice(1) }

    this.onError(text)
  }

  // :hybrid8.debian.local 324 bonbori #general +nt
  // :hybrid8.debian.local 324 bonbori #general +mntl 10
  _onChannelMode (data) {
    const prefix = data.split(' ')[0]
    const text = data.slice(prefix.length + 1)
    this._onModeChange(text)
  }

  // :bonbori!~bonbori@localhost MODE bonbori :+i
  // :bonbori!~bonbori@localhost MODE #general -t
  // :gobusto!~gobusto@localhost MODE #general +o bonbori
  // :gobusto!~gobusto@localhost MODE #general +l 200
  // :gobusto!~gobusto@localhost MODE #general +lob 123 bonbori *!*@*.se
  // :gobusto!~gobusto@localhost MODE #general +blo *!*@*.de 123 bonbori
  _onModeChange (data) {
    let items = data.split(' ').reverse()
    const channel = items.pop()
    const modes = items.pop()

    if (!this._validChannelName(channel)) {
      this._onUserModeChange(channel, modes)
    } else {
      this._onChannelModeChange(channel, modes, items)
    }
  }

  // :bonbori!~bonbori@localhost MODE bonbori :+i
  _onUserModeChange (user, modes) {
    if (!this.onUserFlagChange) { return }

    const value = modes.includes('+')
    if (modes.includes('i')) { this.onUserFlagChange(user, 'invisible', value) }
    if (modes.includes('s')) { this.onUserFlagChange(user, 'serverNotices', value) }
    if (modes.includes('w')) { this.onUserFlagChange(user, 'wallops', value) }
    if (modes.includes('o')) { this.onUserFlagChange(user, 'operator', value) }
  }

  // :gobusto!~gobusto@localhost MODE #general +lob 123 bonbori *!*@*.se
  _onChannelModeChange (channel, modes, items) {
    const value = modes.includes('+') // If not +, then assume -.

    modes.split('').forEach(mode => {
      // The following cases must ALWAYS be handled, since they `.pop()` items:
      if (mode === 'o') {
        const user = items.pop()
        if (this.onMemberFlagChange) { this.onMemberFlagChange(channel, user, 'operator', value) }
      } else if (mode === 'v') {
        const user = items.pop()
        if (this.onMemberFlagChange) { this.onMemberFlagChange(channel, user, 'voice', value) }
      } else if (mode === 'h') {
        const user = items.pop()
        if (this.onMemberFlagChange) { this.onMemberFlagChange(channel, user, 'halfOperator', value) }
      } else if (mode === 'b') {
        const mask = items.pop()
        if (this.onChannelBanChange) { this.onChannelBanChange(channel, mask, value) }
      } else if (mode === 'l') {
        const limit = value ? Number(items.pop()) : null // null = no limit.
        if (this.onChannelFlagChange) { this.onChannelFlagChange(channel, 'userLimit', limit) }
      } else if (mode === 'k') {
        const password = items.pop() // NOTE: Will be "*" if un-set.
        this.onChannelFlagChange(channel, 'password', value ? password : null)
      }

      // None of the cases below modify items, so we can safely skip them:
      if (!this.onChannelFlagChange) { return }
      else if (mode === 'p') { this.onChannelFlagChange(channel, 'private', value) }
      else if (mode === 's') { this.onChannelFlagChange(channel, 'secret', value) }
      else if (mode === 'i') { this.onChannelFlagChange(channel, 'inviteOnly', value) }
      else if (mode === 't') { this.onChannelFlagChange(channel, 'topicProtected', value) }
      else if (mode === 'n') { this.onChannelFlagChange(channel, 'membersOnly', value) }
      else if (mode === 'm') { this.onChannelFlagChange(channel, 'voicedOnly', value) }
      else if (mode === 'a') { this.onChannelFlagChange(channel, 'anonymous', value) }
    })
  }

  // :card.freenode.net 332 bonbori #general :Testing...
  _onTopic (data) {
    if (!this.onTopic) { return }

    const channel = data.split(' ')[1]
    const text = data.slice(data.indexOf(' :') + 2)

    this.onTopic (channel, text)
  }

  // :hybrid8.debian.local 367 hello #world *!*@*.ro foo!~foo@bar.baz 1593935437
  _onBanListItem (data) {
    if (!this.onChannelBanChange) { return }

    const channel = data.split(' ')[1]
    const mask = data.split(' ')[2]

    this.onChannelBanChange(channel, mask, true)
  }

  // :tepper.freenode.net 301 bonbori gobusto :Be right back!
  _onAutoReply (data) {
    if (!this.onMessage) { return }

    const user = data.split(' ')[1]
    const text = data.slice(data.indexOf(' :') + 2)
    this.onMessage('private', 'autoreply', user, text, user)
  }

  // :current!~original@localhost NICK :newname
  _onNick (oldNick, text) {
    if (!this.onUserFlagChange) { return }

    const newNick = text.startsWith(':') ? text.slice(1) : text
    this.onUserFlagChange(oldNick, 'nick', newNick)
  }

  // :gobusto!~gobusto@0:0:0:0:0:0:0:1 JOIN :#bonboriland
  _onJoin (user, text) {
    let channel = text.split(' ')[0]
    if (channel.startsWith(':')) { channel = channel.slice(1) }

    if (user === this.nick) {
      if (this.onJoin) { this.onJoin(channel) }
    } else {
      if (this.onMemberChange) { this.onMemberChange(channel, user, null, true) }
    }
  }

  // :gobusto!~gobusto@0:0:0:0:0:0:0:1 PART #bonboriland :Leaving
  _onLeave (user, text) {
    let channel = text.split(' ')[0]
    if (channel.startsWith(':')) { channel = channel.slice(1) }

    let reason = text.slice(channel.length + 1) || ''
    if (reason.startsWith(':')) { reason = reason.slice(1) }

    if (user === this.nick) {
      if (this.onLeave) { this.onLeave(channel) }
    } else {
      if (this.onMemberChange) { this.onMemberChange(channel, user, reason, false) }
    }
  }

  // :tepper.freenode.net 322 bonbori #reddit 176 :Welcome to #reddit 2000
  _onChannelListItem (data) {
    if (!this.gettingChannelList) { return }

    const name = data.split(' ')[1]
    const users = data.split(' ')[2]
    const topic = data.slice(data.indexOf(' :') + 2)
    this.channelList[name] = { topic: topic, userCount: users }
  }

  _onChannelListEnd () {
    if (!this.gettingChannelList) { return }
    this.gettingChannelList = false

    if (!this.onChannelList) { return }
    this.onChannelList(this.channelList)
  }

  // :card.freenode.net 353 bonbori = #general :bonbori hozuki gobusto
  _onNameList (data) {
    const channel = data.split(' ')[2]
    const names = data.slice(data.indexOf(' :') + 2)

    const users = names.split(' ').forEach(name => {
      let user = name
      let flag = null

      if (name.startsWith('@')) {
        user = name.slice(1)
        flag = 'operator'
      } else if (name.startsWith('%')) {
        user = name.slice(1)
        flag = 'halfOperator'
      } else if (name.startsWith('+')) {
        user = name.slice(1)
        flag = 'voice'
      }

      if (this.onMemberChange) {
        this.onMemberChange(channel, user, 'namelist', true)
      }

      if (this.onMemberFlagChange && flag) {
        this.onMemberFlagChange(channel, user, flag, true)
      }
    })
  }

  // :orwell.freenode.net 311 bonbori hozuki ~hozuki 95.150.81.37 * :Anonymous
  _onWhoIs (data) {
    if (!this.onUserFlagChange) { return }

    const bits = data.split(' ')
    const nick = bits[1]
    const user = bits[2]
    const host = bits[3]
    const mask = `${nick}!${user}@${host}`
    const name = data.slice(data.indexOf(' :') + 2)

    this.onUserFlagChange(nick, 'name', name)
    this.onUserFlagChange(nick, 'mask', mask)
  }

  // :gobusto!~gobusto@95.150.81.37 PRIVMSG #general :Hello, World!
  _onMessage (user, code, data) {
    if (!this.onMessage) { return }

    const channel = data.split(' ')[0]
    let text = data.slice(channel.length + 1)
    if (text.startsWith(':')) { text = text.slice(1) }

    let kind = 'unknown'

    if (code === 'NOTICE') {
      kind = 'notice'
    } else if (code === 'PRIVMSG') {
      kind = 'normal'
    } else if (code === 'TOPIC') {
      kind = 'topic'
    } else if (code === IRC_CODE.MOTD_LINE) {
      kind = 'motd'
    } else if (code.startsWith('4')) {
      kind = 'error'
    } else {
      return // Remove this line to see EVERY kind of message.
    }

    if (user && channel === this.nick) {
      return this.onMessage('private', kind, user, text, user)
    } else if (this._validChannelName(channel)) {
      return this.onMessage('public', kind, user, text, channel)
    }

    return this.onMessage('public', kind, user, text)
  }

  _onClose (event) {
    console.debug('Close:', event.code, event.reason)

    // Websockify rejects plain-text messages, so try switching to binary:
    if (event.code === 1003 && !this.sendAsBinary) {
      const text = 'Websocket proxy does not accept text; retrying as binary...'
      if (this.onMessage) { this.onMessage('public', 'error', null, text) }
      // See "ERROR :Your host is trying to (re)connect too fast -- throttled."
      this.sendAsBinary = true
      window.setTimeout(this._connect.bind(this), 2000) // For "ircd-hybrid".
    } else if (this.onDisconnect) {
      this.onDisconnect(event.code, event.reason)
    }
  }

  _send (text) {
    if (!this.sendAsBinary) { this.socket.send(`${text}\r\n`) }

    // Websockify complains if we send text, so send a binary blob instead:
    this.socket.send(new Blob([`${text}\r\n`], { type: 'text/plain' }))
  }

  _validChannelName (text) {
    return ['^#.', '^&.', '^\\+.', '^!.'].some(c => text.match(c)) // RFC-2811
  }
}

export default IrcConnection
