// Components:
import LoadingScreen from './components/LoadingScreen.js'
import MainScreen from './components/MainScreen.js'
import SignInScreen from './components/SignInScreen.js'

// API:
import IrcConnection from './IrcConnection.js'

// Miscellaneous:
import WEBSOCKET_STATUS from './data/WebsocketStatus.js'

class App extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      connection: null,
      error: null
    }

    this.handleClosePage = this.handleClosePage.bind(this)
    this.handleConnect = this.handleConnect.bind(this)
    this.handleDisconnect = this.handleDisconnect.bind(this)
    this.handleGetChannels = this.handleGetChannels.bind(this)
    this.handleJoin = this.handleJoin.bind(this)
    this.handleLeave = this.handleLeave.bind(this)
    this.handleStartDirectMessage = this.handleStartDirectMessage.bind(this)
    this.handleCloseDirectMessage = this.handleCloseDirectMessage.bind(this)
    this.handleAway = this.handleAway.bind(this)
    this.handleBack = this.handleBack.bind(this)
    this.handleSetTopic = this.handleSetTopic.bind(this)
    this.handleSend = this.handleSend.bind(this)
    this.handleError = this.handleError.bind(this)
    this.handleSetTopicProtected = this.handleSetTopicProtected.bind(this)
    this.handleSetVoicedOnly = this.handleSetVoicedOnly.bind(this)
    this.handleSetInviteOnly = this.handleSetInviteOnly.bind(this)
    this.handleSetMembersOnly = this.handleSetMembersOnly.bind(this)
    this.handleSetHideChannel = this.handleSetHideChannel.bind(this)
  }

  componentDidMount() {
    window.addEventListener('beforeunload', this.handleClosePage)
  }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.handleClosePage)
  }

  handleClosePage (event) {
    if (this.state.connection) { this.state.connection.disconnect() }
  }

  handleConnect (params) {
    event.preventDefault()

    const connection = new IrcConnection(params)

    this.setState({
      connection: connection,
      error: null,
      messages: [],
      channels: {},
      directMessages: {},
      knownUsers: {},
      knownChannels: {},
      knownChannelsCacheTime: null,
      myNick: params.nick,
      loadingMessage: 'Connecting...'
    })

    // NOTE: We set error to `null` in case we had to reconnect after an error!
    connection.onConnect = () => {
      this.setState({ loadingMessage: 'Connected!', error: null })
    }

    connection.onReady = () => this.setState({ loadingMessage: null })

    connection.onDisconnect = (code, reason) => {
      // NOTE: We don't clear "error" here, in case we were kill'd by an IRCop.
      this.setState({ loadingMessage: null, connection: null })

      // Status code 1000 means "the websocket closed normally", so we wouldn't
      // show an error "reason" in this case, even though Websockify sends one.
      if (code === 1000) { return }
      this.setState({ error: reason || WEBSOCKET_STATUS[String(code)] })
    }

    connection.onChannelList = this.channelList.bind(this)
    connection.onJoin = this.joinChannel.bind(this)
    connection.onLeave = this.leaveChannel.bind(this)
    connection.onTopic = this.changeTopic.bind(this)
    connection.onMessage = this.receiveMessage.bind(this)
    connection.onError = (error) => this.setState({ error: error })

    connection.onUserFlagChange = this.onUserFlagChange.bind(this)

    connection.onMemberChange = this.onMemberChange.bind(this)
    connection.onMemberFlagChange = this.onMemberFlagChange.bind(this)

    connection.onChannelFlagChange = this.onChannelFlagChange.bind(this)
    connection.onChannelBanChange = this.onChannelBanChange.bind(this)
  }

  handleDisconnect (event) {
    event.preventDefault()

    this.setState({ loadingMessage: 'Disconnecting...' })
    this.state.connection.disconnect()
  }

  handleGetChannels () {
    const cacheLifetime = 5 * 60 * 1000 // 5 minutes (in milliseconds).
    const currentTime = new Date()
    const lastUpdated = this.state.knownChannelsCacheTime // May be null!

    // The channel list can be quite big, so avoid re-fetching it if we can:
    if (lastUpdated && (currentTime - lastUpdated) < cacheLifetime) {
      return console.log('Using local channel list cache (update skipped)')
    }

    this.state.connection.getChannels()
  }

  handleJoin (channel) {
    this.state.connection.join(channel)
  }

  handleLeave (channel) {
    this.state.connection.leave(channel)
  }

  handleStartDirectMessage (user) {
    if (this.state.directMessages[user]) { return }

    const conversations = { ...this.state.directMessages }
    conversations[user] = []
    this.setState({ directMessages: conversations })
  }

  handleCloseDirectMessage (user) {
    if (!this.state.directMessages[user]) { return }

    const conversations = { ...this.state.directMessages }
    delete conversations[user]
    this.setState({ directMessages: conversations })
  }

  handleAway (message) {
    this.state.connection.away(message)
  }

  handleBack () {
    this.state.connection.back()
  }

  handleSetTopic (channel, topic) {
    this.state.connection.setTopic(channel, topic)
  }

  handleSend (channel, message) {
    this.state.connection.send(channel, message)
  }

  handleError (error) {
    this.setState({ error: error })
  }

  handleSetTopicProtected (channel) {
    const flags = this.state.channels[channel]
    if (!flags) { return }

    this.state.connection.setTopicProtected(channel, !flags.topicProtected)
  }

  handleSetVoicedOnly (channel) {
    const flags = this.state.channels[channel]
    if (!flags) { return }

    this.state.connection.setVoicedOnly(channel, !flags.voicedOnly)
  }

  handleSetInviteOnly (channel) {
    const flags = this.state.channels[channel]
    if (!flags) { return }

    this.state.connection.setInviteOnly(channel, !flags.inviteOnly)
  }

  handleSetMembersOnly (channel) {
    const flags = this.state.channels[channel]
    if (!flags) { return }

    this.state.connection.setMembersOnly(channel, !flags.membersOnly)
  }

  handleSetHideChannel (channel) {
    const flags = this.state.channels[channel]
    if (!flags) { return }

    const value = flags.private || flags.secret // Unset both if either is set.
    this.state.connection.setHideChannel(channel, !value)
  }

  channelList (channels) {
    this.setState({
      knownChannels: channels,
      knownChannelsCacheTime: new Date ()
    })
  }

  onMemberChange (channel, user, reason, value) {
    const oldChannel = this.state.channels[channel]
    if (!oldChannel) { return }

    // Add or remove the user.
    let newUsers = { ...oldChannel.users }
    if (value) {
      newUsers[user] = {}
    } else {
      delete newUsers[user]
    }

    // Update the channel:
    let newChannels = { ...this.state.channels }
    newChannels[channel] = { ...oldChannel, users: newUsers }
    this.setState({ channels: newChannels })

    // Show join/leave events as though they were messages:
    if (!value) {
      this.receiveMessage('public', 'leave', user, reason, channel)
    } else if (reason !== 'namelist') {
      this.receiveMessage('public', 'join', user, '', channel)
    }
  }

  onUserFlagChange (user, flag, value) {
    const userList = { ...this.state.knownUsers }

    if (flag === 'nick') {
      userList[value] = {} // Clear any old data if this nick is being re-used.

      // If it's OUR nick that has been changed, track the new one instead:
      if (user === this.state.myNick) { this.setState({ myNick: value }) }

      // If this user is in any of our channels, update their name there, too:
      Object.keys(this.state.channels).forEach(channel => {
        if (this.state.channels[channel].users[user]) {
          this.onMemberFlagChange(channel, user, flag, value)
        }
      })

      console.log(user, 'is now known as', value) // TODO: Show a message...?
    } else {
      userList[user] = userList[user] ? { ...userList[user] } : {}
      userList[user][flag] = value
    }

    this.setState({ knownUsers: userList })
  }

  onMemberFlagChange (channel, user, flag, value) {
    const oldChannel = this.state.channels[channel]
    if (!oldChannel || !oldChannel.users[user]) { return }

    let newUsers = { ...oldChannel.users }
    if (flag === 'nick') {
      newUsers[value] = newUsers[user]
      delete newUsers[user]
    } else {
      newUsers[user] = { ...oldChannel.users[user] }
      newUsers[user][flag] = value
    }

    let newChannels = { ...this.state.channels }
    newChannels[channel] = { ...oldChannel, users: newUsers }
    this.setState({ channels: newChannels })
  }

  onChannelFlagChange (channel, flag, value) {
    const oldChannel = this.state.channels[channel]
    if (!oldChannel) { return }

    console.log('CHANNEL FLAG CHANGED:', channel, flag, value)

    let newChannels = { ...this.state.channels }
    newChannels[channel] = { ...oldChannel }
    newChannels[channel][flag] = value
    this.setState({ channels: newChannels })
  }

  onChannelBanChange (channel, mask, enabled) {
    console.log('BAN:', mask, enabled ? 'ADDED TO' : 'REMOVED FROM', channel)
  }

  joinChannel (channel) {
    if (this.state.channels[channel]) { return }

    let newChannels = { ...this.state.channels }
    newChannels[channel] = { topic: '', users: {}, messages: [] }
    this.setState({ channels: newChannels })

    this.state.connection.getChannelFlags(channel)
    this.state.connection.getChannelBans(channel)
  }

  leaveChannel (channel) {
    let newChannels = { ...this.state.channels }
    delete newChannels[channel]
    this.setState({ channels: newChannels })
  }

  changeTopic (channel, topic) {
    const oldChannel = this.state.channels[channel]
    if (!oldChannel) { return }

    let newChannels = { ...this.state.channels }
    newChannels[channel] = { ...oldChannel, topic: topic }
    this.setState({ channels: newChannels })
  }

  receiveMessage (visibility, kind, user, text, channel) {
    const message = { kind: kind, user: user, text: text, time: new Date() }

    // Show "Nickname is already in use" if we're still on the loading screen:
    if (kind === 'error' && this.state.loadingMessage) {
      this.setState({ error: text })
    }

    // If we don't know who this user is, query their real name.
    const userInfo = this.state.knownUsers[user] || {}
    if (user && !userInfo.name) { this.state.connection.whoIs(user) }

    if (visibility === 'private') {
      return this.receivePrivateMessage(message, channel)
    } else if (channel) {
      return this.receiveChannelMessage(message, channel)
    }

    this.receiveGlobalMessage(message)
  }

  receiveGlobalMessage (message) {
    const newMessages = this.state.messages.concat([message])
    this.setState({ messages: newMessages })
  }

  receivePrivateMessage (message, user) {
    const conversations = { ...this.state.directMessages }
    conversations[user] = (conversations[user] || []).concat([message])
    this.setState({ directMessages: conversations })
  }

  receiveChannelMessage (message, channel) {
    const oldChannel = this.state.channels[channel]
    if (!oldChannel) { return }

    const newChannels = { ...this.state.channels }
    const newMessages = oldChannel.messages.concat([message])
    newChannels[channel] = { ...oldChannel, messages: newMessages }
    this.setState({ channels: newChannels })

    if (message.kind === 'topic') { this.changeTopic(channel, message.text) }
  }

  render () {
    const e = React.createElement

    if (!this.state.connection) {
      return e(SignInScreen, {
        error: this.state.error,
        onError: this.handleError,
        onSubmit: this.handleConnect
      })
    }
    else if (this.state.loadingMessage) {
      const text = this.state.error || this.state.loadingMessage
      return e(LoadingScreen, { message: text })
    }

    // We could just pass in the nick (and look it up in knownUsers), but:
    const userInfo = this.state.knownUsers[this.state.myNick] || {}

    return e(MainScreen, {
      currentUser: { ...userInfo, nick: this.state.myNick },
      knownUsers: this.state.knownUsers,
      knownChannels: this.state.knownChannels,
      knownChannelsCacheTime: this.state.knownChannelsCacheTime,
      channels: this.state.channels,
      messages: this.state.messages,
      directMessages: this.state.directMessages,
      onGetChannels: this.handleGetChannels,
      onJoin: this.handleJoin,
      onLeave: this.handleLeave,
      onStartDirectMessage: this.handleStartDirectMessage,
      onCloseDirectMessage: this.handleCloseDirectMessage,
      onAway: this.handleAway,
      onBack: this.handleBack,
      onSetTopic: this.handleSetTopic,
      onSetTopicProtected: this.handleSetTopicProtected,
      onSetVoicedOnly: this.handleSetVoicedOnly,
      onSetInviteOnly: this.handleSetInviteOnly,
      onSetMembersOnly: this.handleSetMembersOnly,
      onSetHideChannel: this.handleSetHideChannel,
      onSend: this.handleSend,
      onDisconnect: this.handleDisconnect
    })
  }
}

export default App
