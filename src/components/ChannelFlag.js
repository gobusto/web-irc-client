// A simple channel flag indicator.
function ChannelFlag (props) {
  const e = React.createElement

  const toggle = props.value ? 'on' : 'off'

  return (
    e('button', {
      className: `channel-flag ${props.flag} ${toggle}`,
      disabled: !props.enabled,
      onClick: props.onClick
    },
      `${props.flag} ${toggle}`
    )
  )
}

export default ChannelFlag
