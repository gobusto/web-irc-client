function Avatar (props) {
  const e = React.createElement

  return e('div', { className: 'avatar', 'data-user': props.user })
}

export default Avatar
