// Components:
import Avatar from './Avatar.js'

// The "Account" bit of the left-hand bar.
class UserMenu extends React.Component {
  constructor (props) {
    super(props)

    this.state = { expand: false }

    this.handleToggle = this.handleToggle.bind(this)
  }

  handleToggle (event) {
    event.preventDefault()

    this.setState({ expand: !this.state.expand })
  }

  render () {
    const e = React.createElement

    const body = e('div', { className: 'body' }, this.props.children)

    return (
      e('div', { className: 'user-menu' },
        e('div', { className: 'head' },
          e('button', { className: 'icon', onClick: this.handleToggle },
            e(Avatar, { user: this.props.user.nick })
          ),
          e('div', { className: 'nick' },
            this.props.user.nick
          )
        ),
        this.state.expand ? body : null
      )
    )
  }
}

export default UserMenu
