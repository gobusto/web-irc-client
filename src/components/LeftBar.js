// Components:
import SideBarButton from './SideBarButton.js'
import SideBarItem from './SideBarItem.js'
import UserMenu from './UserMenu.js'

function LeftBar (props) {
  const e = React.createElement

  const channels = props.channels.map(channel => (
    e(SideBarItem, {
      key: channel,
      label: channel,
      active: channel == props.currentChannel,
      onClick: () => props.onFocus(channel)
    },
      e(SideBarButton, {
        label: 'Leave',
        remove: true,
        onClick: () => props.onLeave(channel)
      })
    )
  ))

  const directMessages = props.directMessages.map(user => (
    e(SideBarItem, {
      key: user,
      label: user,
      active: user == props.currentChannel,
      onClick: () => props.onFocus(user)
    },
      e(SideBarButton, {
        label: 'Leave',
        remove: true,
        onClick: () => props.onCloseDirectMessage(user)
      })
    )
  ))

  return e('div', { className: 'left-bar' },
    e(UserMenu, { user: props.currentUser },
      e(SideBarItem, {
        label: `Mark yourself as "${props.currentUser.away ? 'back' : 'away'}"`,
        onClick: props.currentUser.away ? props.onBack : () => props.onAway()
      }),
      e(SideBarItem, {
        label: 'Disconnect',
        onClick: props.onDisconnect
      }),
    ),
    e(SideBarItem, {
      label: 'Server Messages',
      active: !props.currentChannel,
      onClick: () => props.onFocus(null)
    }),
    e(SideBarItem, { label: 'Channels', header: true },
      e(SideBarButton, {
        label: 'Browse',
        create: true,
        onClick: props.onChannelMenu
      })
    ),
    channels,
    e(SideBarItem, { label: 'Direct Messages', header: true },
      e(SideBarButton, {
        label: 'Start',
        create: true
      })
    ),
    directMessages
  )
}

export default LeftBar
