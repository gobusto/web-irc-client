// Represents a "row" in a sidebar.
function SideBarItem (props) {
  const e = React.createElement

  const active = props.active ? 'active' : ''
  const header = props.header ? 'header' : ''

  return (
    e('div', { className: `side-bar-item ${active} ${header}` },
      e('button', { className: 'label', onClick: props.onClick }, props.label),
      props.children
    )
  )
}

export default SideBarItem
