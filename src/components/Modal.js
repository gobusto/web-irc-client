// A generic "overlay" window.
function Modal (props) {
  const e = React.createElement

  return (
    e('div', { className: 'modal-container' },
      e('div', { className: 'modal' },
        e('div', { className: 'modal-head' },
          e('div', { className: 'modal-title' },
            props.title
          ),
          e('button', { className: 'modal-close', onClick: props.onClose },
            'Close'
          )
        ),
        e('div', { className: 'modal-body' },
          props.children
        )
      )
    )
  )
}

export default Modal
