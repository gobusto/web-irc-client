// Labels an input element.
function InputWrapper (props) {
  const e = React.createElement

  return (
    e('label', { className: 'input-wrapper' },
      e('div', { className: 'text' }, props.label),
      e('div', { className: 'hint' }, props.hint),
      e('div', { className: 'main' }, props.children)
    )
  )
}

export default InputWrapper
