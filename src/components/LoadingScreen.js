// Intermission screen.
function LoadingScreen (props) {
  const e = React.createElement

  return (
    e('div', { className: 'loading-screen' },
      props.message || 'Please wait...'
    )
  )
}

export default LoadingScreen
