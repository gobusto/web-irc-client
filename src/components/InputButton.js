// Represents an "add-on" button next to an input element.
function InputButton (props) {
  const e = React.createElement

  // NOTE: Don't use a <button> - pressing return on a <form> would trigger it!
  return (
    e('input', {
      type: 'button',
      value: props.label,
      className: `input-button ${props.primary ? 'primary' : 'general'}`,
      onClick: props.onClick,
      disabled: props.disabled
    })
  )
}

export default InputButton
