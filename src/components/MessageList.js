// Components:
import Message from './Message.js'

// Literally just a list of messages.
class MessageList extends React.Component {
  constructor (props) {
    super(props)

    this.bottom = React.createRef()

    this.scrollDown = this.scrollDown.bind(this)
  }

  scrollDown () {
    this.bottom.current.scrollIntoView()
  }

  componentDidMount() {
    this.scrollDown()
  }

  componentDidUpdate() {
    this.scrollDown()
  }

  render () {
    const e = React.createElement

    let lastUser = undefined // The first message can't have the same author!
    const items = this.props.messages.map((message, i) => {
      const user = this.props.users[message.user]
      const more = message.user === lastUser // Did the same person write this?

      lastUser = message.user // Remember this for next time...
      return e(Message, { key: i, message: message, user: user, more: more })
    })

    return (
      e('div', { className: 'message-list' },
        items,
        e('div', { ref: this.bottom, className: 'bottom' })
      )
    )
  }
}

export default MessageList
