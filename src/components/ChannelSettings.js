// Components:
import Checkbox from './Checkbox.js'

// Channel Settings.
function ChannelSettings (props) {
  const e = React.createElement

  return (
    e('div', { className: 'channel-settings' },
      e(Checkbox, {
        label: 'Topic Protected',
        checked: props.channelInfo.topicProtected,
        onClick: props.onSetTopicProtected,
        description: 'Only channel operators can change the topic.'
      }),
      e(Checkbox, {
        label: 'Invite Only',
        checked: props.channelInfo.inviteOnly,
        onClick: props.onSetInviteOnly,
        description: 'Users cannot join the channel unless invited.'
      }),
      e(Checkbox, {
        label: 'Voiced Only',
        checked: props.channelInfo.voicedOnly,
        onClick: props.onSetVoicedOnly,
        description: 'Only channel operators or voiced users can send messages.'
      }),
      e(Checkbox, {
        label: 'Members Only',
        checked: props.channelInfo.membersOnly,
        onClick: props.onSetMembersOnly,
        description: "Prevents messages from users who aren't in the channel."
      }),
      e(Checkbox, {
        label: 'Hidden',
        checked: props.channelInfo.secret || props.channelInfo.private,
        onClick: props.onSetHideChannel,
        description: 'Prevents the channel from being publicly listed.'
      })
    )
  )
}

export default ChannelSettings
