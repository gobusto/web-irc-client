// Components:
import ChannelFlag from './ChannelFlag.js'
import TopicEditor from './TopicEditor.js'

// Allows changing the topic, etc.
function ChannelControls (props) {
  const e = React.createElement

  return (
    e('div', { className: 'channel-controls' },
      e(ChannelFlag, {
        flag: 'topic-protected',
        value: props.channelInfo.topicProtected,
        enabled: true, // TODO: Should depend on our permissions.
        onClick: props.onSetTopicProtected
      }),
      e(TopicEditor, {
        topic: props.channelInfo.topic,
        enabled: true, // Should depend on our permissions.
        onSetTopic: props.onSetTopic
      }),
      e(ChannelFlag, {
        flag: 'settings',
        enabled: true, // TODO: Should depend on our permissions.
        onClick: props.onOpenChannelSettings
      })
    )
  )
}

export default ChannelControls
