// Components:
import Avatar from './Avatar.js'

function Message (props) {
  const e = React.createElement

  const options = { hour: 'numeric', minute: 'numeric' }
  const formatter = new Intl.DateTimeFormat('en-GB', options)

  const info = props.user || {}
  const name = info.name || props.message.user
  const time = formatter.format(props.message.time)
  const more = props.more ? 'more' : 'start'

  return e('div', { className: `message ${props.message.kind} ${more}` },
    e(Avatar, { user: props.message.user }),
    e('div', { className: 'name' }, name),
    e('div', { className: 'nick' }, props.message.user),
    e('div', { className: 'text' }, props.message.text),
    e('div', { className: 'time' }, time)
  )
}

export default Message
