// Displays a list of channels.
function ChannelList (props) {
  const e = React.createElement

  const channels = Object.keys(props.channels).map(name => (
    e('button', { key: name, className: 'channel-list-item', onClick: () => props.onClick(name) },
      e('div', { className: 'name' }, name),
      e('div', { className: 'users' }, props.channels[name].userCount),
      e('div', { className: 'topic' }, props.channels[name].topic)
    )
  ))

  return (
    e('div', { className: 'channel-list' },
      channels.length ? channels : props.children
    )
  )
}

export default ChannelList
