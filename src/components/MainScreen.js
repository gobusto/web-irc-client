// Components:
import ChannelBrowser from './ChannelBrowser.js'
import ChannelControls from './ChannelControls.js'
import ChannelSettings from './ChannelSettings.js'
import MessageEditor from './MessageEditor.js'
import InfoBar from './InfoBar.js'
import MessageList from './MessageList.js'
import Modal from './Modal.js'
import LeftBar from './LeftBar.js'
import RightBar from './RightBar.js'

// This is the main "chat" screen.
class MainScreen extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      currentChannel: null,
      modalContent: null
    }

    this.handleOpenChannelBrowser = this.handleOpenChannelBrowser.bind(this)
    this.handleOpenChannelSettings = this.handleOpenChannelSettings.bind(this)
    this.handleCloseModal = this.handleCloseModal.bind(this)
    this.handleJoin = this.handleJoin.bind(this)
    this.handleLeave = this.handleLeave.bind(this)
    this.handleCloseDirectMessage = this.handleCloseDirectMessage.bind(this)
    this.handleFocus = this.handleFocus.bind(this)
  }

  handleOpenChannelBrowser () {
    this.setState({ modalContent: 'channel-browser' })
  }

  handleOpenChannelSettings () {
    this.setState({ modalContent: 'channel-settings' })
  }

  handleCloseModal () {
    this.setState({ modalContent: null })
  }

  handleJoin (channel) {
    this.setState({ modalContent: null, currentChannel: channel })
    this.props.onJoin(channel)
  }

  handleLeave (channel) {
    this.setState({ currentChannel: null })
    this.props.onLeave(channel)
  }

  handleCloseDirectMessage (user) {
    this.setState({ currentChannel: null })
    this.props.onCloseDirectMessage(user)
  }

  handleFocus (channel) {
    this.setState({ currentChannel: channel })
  }

  render () {
    const e = React.createElement

    let messages = []
    let channelMenu = null
    let infoBar = null
    let modal = null

    // Show this by default...
    let editor = (
      e(MessageEditor, {
        channel: this.state.currentChannel,
        onSend: this.props.onSend
      })
    )

    if (this.state.modalContent === 'channel-browser') {
      modal = (
        e(Modal, {
          title: 'Find or create a channel',
          onClose: this.handleCloseModal
        },
          e(ChannelBrowser, {
            knownChannels: this.props.knownChannels,
            knownChannelsCacheTime: this.props.knownChannelsCacheTime,
            onGetChannels: this.props.onGetChannels,
            onJoin: this.handleJoin
          })
        )
      )
    }


    if (!this.state.currentChannel) {
      messages = this.props.messages
      editor = null // ...but hide it if we're just looking at server messages.
    } else if (this.props.directMessages[this.state.currentChannel]) {
      messages = this.props.directMessages[this.state.currentChannel]
    } else if (this.props.channels[this.state.currentChannel]) {
      const channelInfo = this.props.channels[this.state.currentChannel]
      messages = channelInfo.messages
      channelMenu = e(RightBar, { channel: channelInfo })

      infoBar = (
        e(InfoBar, null,
          e(ChannelControls, {
            channelInfo: channelInfo,
            onSetTopicProtected: () => this.props.onSetTopicProtected(this.state.currentChannel),
            onSetTopic: (text) => this.props.onSetTopic(this.state.currentChannel, text),
            onOpenChannelSettings: this.handleOpenChannelSettings
          })
        )
      )

      if (this.state.modalContent === 'channel-settings') {
        modal = (
          e(Modal, {
            title: 'Channel Settings',
            onClose: this.handleCloseModal
          },
            e(ChannelSettings, {
              channelInfo: channelInfo,
              onSetTopicProtected: () => this.props.onSetTopicProtected(this.state.currentChannel),
              onSetInviteOnly: () => this.props.onSetInviteOnly(this.state.currentChannel),
              onSetVoicedOnly: () => this.props.onSetVoicedOnly(this.state.currentChannel),
              onSetMembersOnly: () => this.props.onSetMembersOnly(this.state.currentChannel),
              onSetHideChannel: () => this.props.onSetHideChannel(this.state.currentChannel)
            })
          )
        )
      }
    }

    const generalMenu = (
      e(LeftBar, {
        currentUser: this.props.currentUser,
        currentChannel: this.state.currentChannel,
        channels: Object.keys(this.props.channels),
        directMessages: Object.keys(this.props.directMessages),
        onChannelMenu: this.handleOpenChannelBrowser,
        onLeave: this.handleLeave,
        // onDirectMessageMenu: this.handleOpenDirectMessageBrowser,
        onCloseDirectMessage: this.handleCloseDirectMessage,
        onFocus: this.handleFocus,
        onAway: this.props.onAway,
        onBack: this.props.onBack,
        onDisconnect: this.props.onDisconnect
      })
    )

    return (
      e('div', null,
        e('div', { className: 'main-screen' },
          generalMenu,
          channelMenu,
          infoBar,
          editor,
          e(MessageList, { messages: messages, users: this.props.knownUsers })
        ),
        modal
      )
    )
  }
}

export default MainScreen
