// Components:
import EmojiSearch from './EmojiSearch.js'

// Used to compose text messages.
class MessageEditor extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      emojiMenu: false,
      text: ''
    }

    this.handleEmojiMenu = this.handleEmojiMenu.bind(this)
    this.handleInsert = this.handleInsert.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSend = this.handleSend.bind(this)

    this.textElement = React.createRef()
  }

  handleEmojiMenu (event) {
    this.setState({ emojiMenu: !this.state.emojiMenu })
  }

  handleInsert (value) {
    const elem = this.textElement.current
    elem.focus()

    const prefix = elem.value.slice(0, elem.selectionStart)
    const suffix = elem.value.slice(elem.selectionEnd)
    const result = [prefix, value, suffix].join('')
    this.setState({ text: result, emojiMenu: false })
  }

  handleChange (event) {
    this.setState({ text: event.target.value })
  }

  handleSend (event) {
    event.preventDefault()

    // Don't allow "" (or "    ") to be sent.
    if (!this.state.text.trim()) { return }

    this.props.onSend(this.props.channel, this.state.text)
    this.setState({ text: '' })
  }

  render () {
    const e = React.createElement

    const emojiSearch = this.state.emojiMenu ? (
      e(EmojiSearch, { onSelect: this.handleInsert })
    ) : null

    return (
      e('div', { className: 'message-editor' },
        e('form', { className: 'message-form', onSubmit: this.handleSend },
          e('input', {
            type: 'button',
            value: '🙂',
            onClick: this.handleEmojiMenu,
            className: `emoji-menu ${this.state.emojiMenu ? 'open' : 'closed'}`
          }),
          e('input', {
            ref: this.textElement,
            className: 'text',
            value: this.state.text,
            onChange: this.handleChange,
            autoFocus: true
          }),
          e('button', { className: 'send' }, 'Send')
        ),
        emojiSearch
      )
    )
  }
}

export default MessageEditor
