// Checkbox.
function Checkbox (props) {
  const e = React.createElement

  const toggleClass = `checkbox-toggle ${props.checked ? 'on' : 'off'}`

  return (
    e('div', { className: 'checkbox' },
      e('div', { className: 'checkbox-label' },
        props.label
      ),
      e('button', { className: toggleClass, onClick: props.onClick },
        props.checked ? 'On' : 'Off'
      ),
      e('div', { className: 'checkbox-description' },
        props.description
      )
    )
  )
}

export default Checkbox
