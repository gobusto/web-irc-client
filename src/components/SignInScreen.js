// Components:
import ErrorMessage from './ErrorMessage.js'
import InputWrapper from './InputWrapper.js'
import InputButton from './InputButton.js'

// This is what the user sees before they connect.
class SignInScreen extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      proxy: 'ws://127.0.0.1:1337',
      host: '',
      port: '',
      pass: '',
      nick: '',
      name: '',
      advanced: false
    }

    this.handleProxyChange = this.handleProxyChange.bind(this)
    this.handleHostChange = this.handleHostChange.bind(this)
    this.handlePortChange = this.handlePortChange.bind(this)
    this.handlePassChange = this.handlePassChange.bind(this)
    this.handleNickChange = this.handleNickChange.bind(this)
    this.handleNameChange = this.handleNameChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleAdvancedToggle = this.handleAdvancedToggle.bind(this)
  }

  handleProxyChange (event) { this.setState({ proxy: event.target.value }) }
  handleHostChange (event) { this.setState({ host: event.target.value }) }
  handlePortChange (event) { this.setState({ port: event.target.value }) }
  handlePassChange (event) { this.setState({ pass: event.target.value }) }
  handleNickChange (event) { this.setState({ nick: event.target.value }) }
  handleNameChange (event) { this.setState({ name: event.target.value }) }

  handleSubmit (event) {
    event.preventDefault()

    const ws = 'ws://\\w'
    const wss = 'wss://\\w'

    if (!this.state.proxy.match(ws) && !this.state.proxy.match(wss)) {
      return this.props.onError('You must specify a websockets proxy')
    } else if (!this.state.nick) {
      return this.props.onError('You must specify a nickname')
    }

    this.props.onSubmit(this.state)
  }

  handleAdvancedToggle (event) {
    event.preventDefault()

    if (!this.state.advanced) { return this.setState({ advanced: true }) }
    this.setState({ advanced: false, host: '', port: '', pass: '' })
  }

  render () {
    const e = React.createElement

    const advancedOptions = (
      e('div', { className: 'advanced-options' },
        e('div', { className: 'irc-host' },
          e(InputWrapper, { label: 'IRC host', hint: 'Required by some proxies' },
            e('input', {
              placeholder: 'irc.example.com',
              value: this.state.host,
              onChange: this.handleHostChange
            }),
            e('input', {
              placeholder: '6667',
              value: this.state.port,
              onChange: this.handlePortChange,
              type: 'number',
              min: 1,
              max: 65535
            })
          ),
        ),
        e('div', { className: 'server-password' },
          e(InputWrapper, { label: 'Server password', hint: 'Required by some hosts' },
            e('input', {
              placeholder: 'hunter2',
              type: 'password',
              value: this.state.pass,
              onChange: this.handlePassChange
            })
          )
        )
      )
    )

    return (
      e('form', { className: 'sign-in-screen', onSubmit: this.handleSubmit },
        e('div', { className: 'error' },
          e(ErrorMessage, { text: this.props.error }),
        ),
        e('div', { className: 'websocket-proxy' },
          e(InputWrapper, { label: 'Websocket proxy', hint: 'Required' },
            e('input', {
              placeholder: 'ws://proxy.example.com:1337',
              value: this.state.proxy,
              onChange: this.handleProxyChange
            }),
            e(InputButton, {
              label: this.state.advanced ? 'Show less' : 'Show more',
              onClick: this.handleAdvancedToggle
            })
          )
        ),
        this.state.advanced ? advancedOptions : null,
        e('div', { className: 'nick' },
          e(InputWrapper, { label: 'Nickname', hint: '9 characters max' },
            e('input', {
              placeholder: 'jsmith',
              value: this.state.nick,
              onChange: this.handleNickChange,
              maxLength: 9,
              autoFocus: true
            })
          )
        ),
        e('div', { className: 'name' },
          e(InputWrapper, { label: 'Full name', hint: 'Optional' },
            e('input', {
              placeholder: 'John Smith',
              value: this.state.name,
              onChange: this.handleNameChange
            })
          )
        ),
        e('div', { className: 'actions' },
          e('button', { className: 'connect-button' }, 'Connect')
        )
      )
    )
  }
}

export default SignInScreen
