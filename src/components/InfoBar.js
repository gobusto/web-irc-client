// The top bar.
function InfoBar (props) {
  const e = React.createElement

  return e('div', { className: 'info-bar' }, props.children)
}

export default InfoBar
