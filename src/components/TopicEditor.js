// Edits the topic.
class TopicEditor extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      edit: false,
      text: ''
    }

    this.handleStartEdit = this.handleStartEdit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
  }

  handleStartEdit (event) {
    this.setState({ edit: true, text: this.props.topic })
  }

  handleChange (event) {
    this.setState({ text: event.target.value })
  }

  handleSubmit (event) {
    event.preventDefault()

    this.setState({ edit: false })
    this.props.onSetTopic(this.state.text)
  }

  handleCancel (event) {
    event.preventDefault()

    this.setState({ edit: false })
  }

  render () {
    const e = React.createElement

    if (this.state.edit) {
      return (
        e('div', { className: 'topic-editor' },
          e('form', { className: 'topic-form', onSubmit: this.handleSubmit },
            e('input', {
              className: 'topic-input',
              value: this.state.text,
              onChange: this.handleChange
            })
          ),
          e('button', { className: 'edit', onClick: this.handleCancel },
            'Cancel'
          )
        )
      )
    }

    return (
      e('div', { className: 'topic-editor' },
        e('div', { className: 'topic-text' }, this.props.topic),
        e('button', { className: 'edit', onClick: this.handleStartEdit },
          'Edit'
        )
      )
    )
  }
}

export default TopicEditor
