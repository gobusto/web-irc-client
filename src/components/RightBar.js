// The right-hand bar when viewing a channel.
function RightBar (props) {
  const e = React.createElement

  const items = Object.keys(props.channel.users).map(userName => {
    const userInfo = props.channel.users[userName]

    return (
      e('div', { key: userName, className: 'channel-user' },
        userInfo.operator ? e('div', { className: 'operator' }, '@') : null,
        userInfo.halfOperator ? e('div', { className: 'half-operator' }, '%') : null,
        userInfo.voice ? e('div', { className: 'voice' }, '+') : null,
        e('div', { className: 'nick' }, userName)
      )
    )
  })

  return e('div', { className: 'right-bar' }, items)
}

export default RightBar
