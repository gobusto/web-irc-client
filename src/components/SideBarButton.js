// Used for the buttons on side-bar items.
function SideBarButton (props) {
  const e = React.createElement

  const create = props.create ? 'create' : ''
  const remove = props.remove ? 'remove' : ''

  return (
    e('button', {
      className: `side-bar-button ${create} ${remove}`,
      onClick: props.onClick
    },
      props.label
    )
  )
}

export default SideBarButton
