function ErrorMessage (props) {
  const e = React.createElement

  if (!props.text) { return null }

  return e('div', { className: 'error-message' }, props.text)
}

export default ErrorMessage
