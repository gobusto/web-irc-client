// This is our "database":
import EMOJI from '../data/Emoji.js'

// writeme
class EmojiSearch extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      group: Object.keys(EMOJI)[0]
    }

    this.handleChangeGroup = this.handleChangeGroup.bind(this)
  }

  handleChangeGroup (group) {
    this.setState({ group: group })
  }

  render () {
    const e = React.createElement

    const groups = Object.keys(EMOJI).map(group => (
      e('input', {
        key: group,
        value: group,
        type: 'button',
        onClick: () => this.handleChangeGroup(group),
        className: `group ${group === this.state.group ? 'active' : 'inactive'}`
      })
    ))

    const items = Object.keys(EMOJI[this.state.group]).map(emoji =>
      e('input', {
        key: emoji,
        value: emoji,
        type: 'button',
        className: 'item',
        onClick: () => this.props.onSelect(emoji)
      })
    )

    return (
      e('div', { className: 'emoji-search' },
        e('div', { className: 'groups' }, groups),
        e('div', { className: 'items' }, items)
      )
    )
  }
}

export default EmojiSearch
