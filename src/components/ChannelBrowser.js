// Components:
import InputWrapper from './InputWrapper.js'
import InputButton from './InputButton.js'
import ChannelList from './ChannelList.js'

// Join or create a channel.
class ChannelBrowser extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      text: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSelect = this.handleSelect.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    this.props.onGetChannels()
  }

  valid () {
    // See https://tools.ietf.org/html/rfc2811
    return ['^#.', '^&.', '^\\+.', '^!.'].some(re => this.state.text.match(re))
  }

  handleChange (event) {
    this.setState({ text: event.target.value.trim() }) // Remove any spaces!
  }

  handleSelect (value) {
    this.setState({ text: value })
  }

  handleSubmit (event) {
    event.preventDefault()

    // Channels (usually) start with # or & followed by (almost) anything else:
    if (this.valid()) { this.props.onJoin(this.state.text) }
  }

  render () {
    const e = React.createElement

    const channelNames = Object.keys(this.props.knownChannels)
    const action = channelNames.includes(this.state.text) ? 'Join' : 'Create'

    let placeholder = 'Type something to search...'
    if (this.state.text) { placeholder = 'No matches found' }

    let cacheText = 'Fetching channels from server...'
    if (this.props.knownChannelsCacheTime) {
      cacheText = `Last cached: ${this.props.knownChannelsCacheTime}`
    }

    // Listing every channel at once can get VERY slow, so filter what we show:
    let results = channelNames.filter(name => (
      this.state.text && name.startsWith(this.state.text)
    ))

    // Build a (filtered) copy of the (complete) "known channels" list:
    let channels = {}
    results.sort().slice(0, 20).forEach(channelName => {
      channels[channelName] = this.props.knownChannels[channelName]
    })

    return (
      e('div', { className: 'channel-browser' },
        e('form', { className: 'create-or-join', onSubmit: this.handleSubmit },
          e(InputWrapper, {
            label: 'Channel name',
            hint: 'Usually starts with #'
          },
            e('input', {
              value: this.state.text,
              onChange: this.handleChange,
              autoFocus: true
            }),
            e(InputButton, {
              label: action,
              primary: true,
              disabled: !this.valid(),
              onClick: this.handleSubmit
            })
          )
        ),
        e('div', { className: 'known-channels' },
          e(ChannelList, { channels: channels, onClick: this.handleSelect },
            placeholder
          )
        ),
        e('div', { className: 'cache-time' },
          cacheText
        )
      )
    )
  }
}

export default ChannelBrowser
