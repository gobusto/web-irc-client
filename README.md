Web IRC Client
--------------

A browser-based IRC client.

This is basically just an experiment, so please don't expect too much!

Using a websocket proxy
-----------------------

The easiest option is to use [websockify](https://github.com/novnc/websockify):

    websockify 0.0.0.0:1337 irc.freenode.org:6667

This will forward _all_ websocket requests to the specified host/port; the ones
requested by the client are ignored.

Alternatively, [ws-tcp-bridge](https://gitlab.com/gobusto/ws-tcp-bridge) (which
I wrote) _does_ respect the host/port specified by the client; however, this is
still rather experimental, so expect to run into bugs.

TODO
----

Basic things, required in order for this client to be considered "finished":

+ Add an interface for adding/removing bans.
+ Disable things like the edit topic button if we don't have permission for it.
+ Support the OPER command.
+ Allow setting a user limit.
+ Allow setting a channel password.
+ Allow joining password-protected channels.
+ Properly handle `/me something` messages.
+ Handle quit events (and messages).
+ Get the direct messages "+" button working.
+ Allow users to change their nickname (without just reconnecting).
+ NickServ doesn't get WHOIS'd + `IDENTIFY <password>` isn't censored.
+ Handle `SecurityError: The operation is insecure` for `ws://` on `https://`.

Enhancements
------------

Nice-to-have features:

+ Possibly support `:discord:` emoji?
+ Allow text to be formatted (as markdown)?
+ Allow file uploads (to a third-party server) so that links can be shared?
+ Display images (or links with OGP metadata) inline?
+ Allow users to specify an avatar, somehow.
+ Show URLs as hyperlinks.

Notes
-----

ircd-hybrid includes several extra modes:

https://fossies.org/linux/ircd-hybrid/doc/modes.txt

Additionally, it allows both `+p` and `+s` to be set at the same time (which is
intentional, but inconsistent with RFC-2811).

Some "extension" flags have conflicting meanings on different IRCd servers:

https://www.alien.net.au/irc/chanmodes.html

Finally, support for various IRCv3 features would be cool: https://ircv3.net/

Technical References
--------------------

Documents describing the IRC protocol:

+ https://tools.ietf.org/html/rfc1459
+ https://tools.ietf.org/html/rfc2810
+ https://tools.ietf.org/html/rfc2811
+ https://tools.ietf.org/html/rfc2812
+ https://tools.ietf.org/html/rfc2813
+ https://tools.ietf.org/html/rfc7194
+ https://modern.ircdocs.horse/

Documents describing emoji:

+ https://unicode.org/emoji/charts/emoji-ordering.txt
+ https://unicode.org/Public/emoji/13.0/emoji-test.txt

Similar IRC clients
-------------------

Other browser-based, open-source IRC clients include:

+ https://kiwiirc.com/
+ https://thelounge.chat/

License
-------

Copyright (c) 2020 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
